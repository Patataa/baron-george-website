import './css/global.scss';
import './index.html';

window.onload = () => {
    const flagParticle = document.getElementById('flagRef');
    const particlesZone = document.getElementById('particles');
    console.log(flagParticle);
    let tick = 0;
    const delta = 1000 / 60;
    let particles = [];
    const speedY = 0.5;
    const vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
    const vh = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);
    console.log(vw);
    let {mouseY, mouseX} = 0;

    const trackMouse = (e) => {
        mouseX = e.clientX;
        mouseY = e.clientY;
    };

    document.addEventListener('mousemove', trackMouse);

    const createParticle = (count) => {
        for(let i = 0; i < count; ++i) {
            const copy = flagParticle.cloneNode(true);
            copy.style.position = "fixed";
            const initX = 90 + (Math.random()*vw) ;
            const randomXVariant = Math.random() * 3.14;
            copy.style.left = initX +'px';
            copy.style.top = -60 +'px';
            copy.style.display = 'block';
            particlesZone.appendChild(copy);
            const data = {
                randomXVariant,
                initX,
                node: copy,
                x: initX,
                y: -60,
            };
            particles.push(data);
        }
    }
    
    const maxVh = vh + 60;
    const garbageParticles = () => {
        console.log('Garbage Particles: ', particles.length);
        const filtered = [];
        particles.forEach((p) => {
            if (p.y > maxVh) {
                
                particlesZone.removeChild(p.node);
            } else {
                filtered.push(p);
            }
        });
        particles = filtered;
    };

    setInterval(() => {
        createParticle(3);
    }, 3500);

    setInterval(() => {
        garbageParticles();
    }, 5000)
    setInterval(()=> {
        particles.forEach( particle => {
            particle.x = particle.x + Math.cos(tick * 0.03 + particle.randomXVariant);
            particle.y = particle.y + (speedY);
            particle.node.style.left = particle.x + 'px';
            particle.node.style.top = particle.y + 'px';
        });
        tick++;
    }, delta);
    
};